﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoNotes.Features.Notes;
using Microsoft.Extensions.Options;
using MongoNotes.Options;
using MongoDB.Driver.Linq;
using MongoDB.Bson;
using System.IO;
using MongoDB.Driver.GridFS;

namespace MongoNotes.Database
{
    public class NoteRepository : INoteRepository
    {
        private readonly NoteContext _context;

        public NoteRepository(IOptions<MongoSettings> settings)
        {
            _context = new NoteContext(settings);
        }

        public async Task AddNote(Note item)
        {
            await _context.Notes.InsertOneAsync(item);
        }

        public async Task<string> DownloadFile(string id)
        {
            var objectId = new ObjectId(id);
            GridFSFileInfo<ObjectId> info = null;
            try
            {
                using (var stream = await _context.Bucket.OpenDownloadStreamAsync(objectId))
                {
                    info = stream.FileInfo;
                }

                return info.Filename;
            }
            catch (Exception)
            {
                return "Not Found";
            }
        }

        public async Task<IEnumerable<Note>> GetAllNotes()
        {
            return await _context.Notes.Find(_ => true).ToListAsync();
        }

        public async Task<Note> GetNote(string id)
        {
            var filter = Builders<Note>.Filter.Eq("Id", id);
            return await _context.Notes.Find(filter).FirstOrDefaultAsync();
        }

        public void RemoveAllNotes()
        {
            _context.Notes.DeleteMany(_ => true);
        }

        public async Task<DeleteResult> RemoveNote(string id)
        {
            return await _context.Notes.DeleteOneAsync(Builders<Note>.Filter.Eq("Id", id));
        }

        public async Task<UpdateResult> UpdateNote(string id, string title)
        {
            var filter = Builders<Note>.Filter.Eq(s => s.Id, id);
            var update = Builders<Note>.Update.Set(s => s.Title, title).CurrentDate(s => s.ModifiedOn);

            return await _context.Notes.UpdateOneAsync(filter, update);
        }

        public async Task<ObjectId> UploadFile(string filename, Stream stream)
        {
            return await _context.Bucket.UploadFromStreamAsync(filename, stream);
        }

        public async Task<ReplaceOneResult> UpsertNote(string id, Note note)
        {
            return await _context
                .Notes
                .ReplaceOneAsync(n => n.Id.Equals(id), note, new UpdateOptions { IsUpsert = true });
        }
    }
}
