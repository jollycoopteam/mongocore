﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;
using MongoNotes.Features.Notes;
using MongoNotes.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MongoNotes.Database
{
    public class NoteContext
    {
        private readonly IMongoDatabase _database = null;
        private readonly IGridFSBucket _bucket = null;

        public NoteContext(IOptions<MongoSettings> settings)
        {
            var client = new MongoClient(settings.Value.ConnectionString);

            if (client != null)
            {
                _database = client.GetDatabase(settings.Value.Database);

                var options = new GridFSBucketOptions()
                {
                    BucketName = "dawg_imges",
                    ChunkSizeBytes = 104876
                };

                _bucket = new GridFSBucket(_database, options);
            }
        }

        public IMongoCollection<Note> Notes => _database.GetCollection<Note>("Note");
        public IGridFSBucket Bucket => _bucket;
    }
}
