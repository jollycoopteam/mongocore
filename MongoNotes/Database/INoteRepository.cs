﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoNotes.Features.Notes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MongoNotes.Database
{
    public interface INoteRepository
    {
        Task<IEnumerable<Note>> GetAllNotes();
        Task<Note> GetNote(string id);
        Task AddNote(Note item);
        Task<DeleteResult> RemoveNote(string id);
        Task<UpdateResult> UpdateNote(string id, string title);
        Task<ReplaceOneResult> UpsertNote(string id, Note note);
        Task<ObjectId> UploadFile(string filename, Stream stream);
        Task<String> DownloadFile(string id);
        void RemoveAllNotes();
    }
}
