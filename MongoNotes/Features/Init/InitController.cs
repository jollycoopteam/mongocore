﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MongoNotes.Database;
using MongoNotes.Features.Notes;

namespace MongoNotes.Features.Init
{
    [Route("api/[controller]")]
    public class InitController : Controller
    {
        private readonly INoteRepository _noteRepository;

        public InitController(INoteRepository noteRepository)
        {
            _noteRepository = noteRepository;
        }

        // Call an initialization - api/system/init
        [HttpGet("{setting}")]
        public string Get(string setting)
        {
            if (setting == "init")
            {
                _noteRepository.RemoveAllNotes();
                _noteRepository.AddNote(new Note()
                {
                    Id = "1",
                    Title = "Test note 1",
                    Addition = "Buy milk",
                    CreatedOn = DateTime.Now,
                    ModifiedOn = DateTime.Now,
                    UserId = 1
                });
                _noteRepository.AddNote(new Note()
                {
                    Id = "2",
                    Title = "Test note 2",
                    Addition = "Buy vinegar",
                    CreatedOn = DateTime.Now,
                    ModifiedOn = DateTime.Now,
                    UserId = 1
                });
                _noteRepository.AddNote(new Note()
                {
                    Id = "3",
                    Title = "Test note 3",
                    Addition = "Buy ketchup",
                    CreatedOn = DateTime.Now,
                    ModifiedOn = DateTime.Now,
                    UserId = 2
                });
                _noteRepository.AddNote(new Note()
                {
                    Id = "4",
                    Title = "Test note 4",
                    Addition = "Buy popatoes",
                    CreatedOn = DateTime.Now,
                    ModifiedOn = DateTime.Now,
                    UserId = 2
                });

                return "Done";
            }

            return "Unknown";
        }
    }
}
