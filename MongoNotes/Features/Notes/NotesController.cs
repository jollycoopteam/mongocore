﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MongoNotes.Database;
using MongoDB.Bson;
using Microsoft.AspNetCore.Http;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MongoNotes.Features.Notes
{
    [Route("api/[controller]")]
    [Consumes("application/json", "multipart/form-data")]
    public class NotesController : Controller
    {
        private readonly INoteRepository _noteRepository;

        public NotesController(INoteRepository noteRepository)
        {
            _noteRepository = noteRepository;
        }

        private async Task<IEnumerable<Note>> GetNoteInternal()
        {
            return await _noteRepository.GetAllNotes();
        }

        private async Task<Note> GetNoteByIdInternal(string id)
        {
            return await _noteRepository.GetNote(id) ?? new Note();
        }


        [HttpGet]
        public async Task<IEnumerable<Note>> Get()
        {
            return await GetNoteInternal();
        }

        [HttpGet("{id}")]
        public async Task<Note> Get(string id)
        {
            return await GetNoteByIdInternal(id);
        }

        [HttpPost]
        public void Post([FromBody]string value)
        {
            _noteRepository.AddNote(new Note()
            {
                Title = value,
                CreatedOn = DateTime.UtcNow,
                ModifiedOn = DateTime.UtcNow
            });
        }

        // PUT api/notes/5
        [HttpPut("{id}")]
        public void Put(string id, [FromBody]string value)
        {
            _noteRepository.UpdateNote(id, value);
        }

        [HttpPut("upsert")]
        public Task Put([FromBody] Note note)
        {
            return _noteRepository.UpsertNote(note.Id,note);
        }

        [HttpDelete("{id}")]
        public void Delete(string id)
        {
            _noteRepository.RemoveNote(id);
        }


        [HttpPost("upload")]
        public async Task<ObjectId> UploadFile(IFormFile file)
        {
            try
            {
                var stream = file.OpenReadStream();
                var filename = file.FileName;
                return await _noteRepository.UploadFile(filename, stream);
            }
            catch (Exception ex)
            {
                return new ObjectId(ex.ToString());
            }
        }


        [HttpGet("download/{id}")]
        public async Task<String> GetFileInfo(string id)
        {
            return await _noteRepository.DownloadFile(id);
        }
    }
}
