﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MongoNotes.Features.Notes
{
    public class Note
    {
        [BsonId]
        public string Id { get; set; }
        public string Title { get; set; }
        public string Addition { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int UserId { get; set; }
    }
}
